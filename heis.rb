# -*- coding: utf-8 -*-

# 概要
# >Heis36 （歩兵３６）
# >２０x２０の格子盤面で、３歩まで歩ける歩兵３６対３６で試合する。
# >HPは各２。自分のターンの１攻撃で１減る。その場は反撃なし。２兵対１で連続攻撃すればダメージなしで１体駆逐。

GAME_TITLE="Heis36"

class Piece
  attr_accessor :x,:y,:hp,:moved

  def initialize(x,y,hp)
    @x,@y,@hp,@moved=x,y,hp,false
  end
end

def draw_board(piece,mapsize)
  mapsize.times.each do |i|
    if i/10!=0 then print"#{i/10} "
    else print"  "
    end
  end
  print"\n"
  mapsize.times.each do |i|
    print"#{i%10} "
  end
  print"\n"
  #二重ループでボード描画
  mapsize.times do |j|
    j=(mapsize-1)-j
    mapsize.times do |i|
      piece[0].length.times do |p|
        #そこにPlayer0の駒があるとき
        if piece[0][p].x==i and piece[0][p].y==j
          case piece[0][p].hp
            when 2 then
            print "A"
            when 1 then
            print "a"
          end
          if piece[0][p].moved then print")"
          else print" "
          end
          break
        #そこにPlayer1の駒があるとき
        elsif piece[1][p].x==i and piece[1][p].y==j
          case piece[1][p].hp
            when 2 then
            print "B"
            when 1 then
            print "b"
          end
          if piece[1][p].moved then print")"
          else print" "
          end
          break
        end
        #どの駒もそこになかったとき
        if p==(piece[0].length-1) then print"- " end
      end
    end
    print"#{j}\n"
  end
end

def movable_search(piece,cur_x,cur_y,dest_x,dest_y,turn_player,move_power)
  #まず, 移動先が有効か
  if cur_x<0 or 20<=cur_x or cur_y<0 or 20<=cur_y
    return false
  end
  piece[0].length.times do |p|
    if cur_x==piece[(turn_player+1)%piece.length][p].x\
      and cur_y==piece[(turn_player+1)%piece.length][p].y
      return false
    end
  end
  #目的地到達でスタックしていなければtrue
  if cur_x==dest_x and cur_y==dest_y
    piece[0].length.times do |p|
      if cur_x==piece[turn_player][p].x\
        and cur_y==piece[turn_player][p].y
        return false
      end
    end
    return true
  end
  #移動力を1消費して1マス移動, 子ノードにtrueがあればやはりtrue
  if move_power>0
    if movable_search(piece,cur_x-1,cur_y,dest_x,dest_y,turn_player,move_power-1)\
      or movable_search(piece,cur_x+1,cur_y,dest_x,dest_y,turn_player,move_power-1)\
      or movable_search(piece,cur_x,cur_y-1,dest_x,dest_y,turn_player,move_power-1)\
      or movable_search(piece,cur_x,cur_y+1,dest_x,dest_y,turn_player,move_power-1)
      return true
    end
  end
  return false
end

def damage(piece,picked,gap_x,gap_y,turn_player,atked_player)
  piece[0].length.times do |p|
    if piece[atked_player][p].x==piece[turn_player][picked].x+gap_x\
      and piece[atked_player][p].y==piece[turn_player][picked].y+gap_y
      piece[atked_player][p].hp-=1
      #被攻撃ユニット死亡時, (-1, -1)に飛ばす
      if piece[atked_player][p].hp==0
        piece[atked_player][p].x=-1
        piece[atked_player][p].y=-1
        return -1
      end
    end
  end
  return 0
end

class Heis
  #ゲームの基本パラメータの設定値
  PLAYERS,PIECES,MAPSIZE,HP,MOVE_POWER=2,36,20,2,3
  #駒オブジェクトを格納する配列
  piece=Array.new(PLAYERS).map{Array.new(PIECES)}
  #初期位置を指定して駒オブジェクト生成
  PIECES.times do |p|
    piece[0][p]=Piece.new((MAPSIZE-1)-p%6,p/6,HP)
    piece[1][p]=Piece.new(p%6,(MAPSIZE-1)-p/6,HP)
  end

  puts GAME_TITLE
  puts "OUTBREAK!!"
  living_soldier=[PIECES,PIECES]
  turn_counter=0
  turn_player=0
  #プレイヤいずれかの生存駒が0になるまで繰り返し
  while living_soldier.min>0
    puts "[Turn #{turn_counter}] Player#{turn_player}'s Turn"
    while true
      draw_board(piece,MAPSIZE)
      #操作駒の指定
      puts"Input '-1' to End Turn"
      print"Pick X: "
      pick_x=gets.to_i
      if pick_x==-1 then break end
      print"Pick Y: "
      pick_y=gets.to_i
      if pick_y==-1 then break end
      picked=nil
      PIECES.times do |p|
        if piece[turn_player][p].x==pick_x and piece[turn_player][p].y==pick_y
          picked=p
          break
        end
      end
      if picked==nil or piece[turn_player][picked].moved==true
        puts"--Recommand!--"
        redo
      end
      #移動先の指定
      print"Destination X: "
      dest_x=gets.to_i
      print"Destination Y: "
      dest_y=gets.to_i
      #移動可能なら移動
      if !(piece[turn_player][picked].x==dest_x and piece[turn_player][picked].y==dest_y)
        if movable_search(piece,piece[turn_player][picked].x,piece[turn_player][picked].y\
                          ,dest_x,dest_y,turn_player,MOVE_POWER)
          piece[turn_player][picked].x=dest_x
          piece[turn_player][picked].y=dest_y
        else
          puts"--Recommand!--"
          redo
        end
      end
      piece[turn_player][picked].moved=true
      #攻撃方向の指定
      print"'w', 'a', 's', 'd' to Attack: "
      atk_dir=gets.chomp
      case atk_dir
      when "w"
        living_soldier[(turn_player+1)%PLAYERS]+=\
        damage(piece,picked,0,1,turn_player,(turn_player+1)%PLAYERS)
      when "a"
        living_soldier[(turn_player+1)%PLAYERS]+=\
        damage(piece,picked,-1,0,turn_player,(turn_player+1)%PLAYERS)
      when "s"
        living_soldier[(turn_player+1)%PLAYERS]+=\
        damage(piece,picked,0,-1,turn_player,(turn_player+1)%PLAYERS)
      when "d"
        living_soldier[(turn_player+1)%PLAYERS]+=\
        damage(piece,picked,1,0,turn_player,(turn_player+1)%PLAYERS)
      end
    end
    #ターン移行処理
    PIECES.times do |p|
      piece[turn_player][p].moved=false
    end
    turn_player=(turn_player+1)%PLAYERS
    turn_counter+=1
  end
  winner=living_soldier.index(living_soldier.max)
  puts"Player#{winner} WON!!"
end
